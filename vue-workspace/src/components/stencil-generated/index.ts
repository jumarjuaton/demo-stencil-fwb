/* eslint-disable */
/* tslint:disable */
/* auto-generated vue proxies */
import { defineContainer } from './vue-component-lib/utils';

import type { JSX } from '@component-library/dist/components';

import { defineCustomElements } from '@component-library/dist/components/dist/loader';

defineCustomElements();

export const AiButton = /*@__PURE__*/ defineContainer<JSX.AiButton>('ai-button', undefined, [
  'color',
  'size',
  'type',
  'disabled',
  'iconOnly',
  'iconLeft',
  'iconRight',
  'circle'
]);


export const AiIcon = /*@__PURE__*/ defineContainer<JSX.AiIcon>('ai-icon', undefined, [
  'icon',
  'size',
  'color'
]);

