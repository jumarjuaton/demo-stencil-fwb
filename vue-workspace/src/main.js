import Vue from 'vue';
import App from './App.vue';

import { applyPolyfills, defineCustomElements } from 'component-library/dist/loader';
import 'component-library/dist/component-library/component-library.css';

Vue.config.productionTip = false;

// Tell Vue to ignore all components defined in the test-components
// package. The regex assumes all components names are prefixed
// 'test'
Vue.config.ignoredElements = [/ai-\w*/];

// Bind the custom elements to the window object
applyPolyfills().then(() => {
  defineCustomElements();
});

new Vue({
  render: h => h(App)
}).$mount('#app');