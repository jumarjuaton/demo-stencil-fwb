import { NgModule } from '@angular/core';

import { AiButton } from './stencil-generated/proxies';
import { AiTextInput } from './stencil-generated/proxies';
import { AiDropdown } from './stencil-generated/proxies';

@NgModule({
  declarations: [
    AiButton,
    AiTextInput,
    AiDropdown
  ],
  imports: [
  ],
  exports: [
    AiButton,
    AiTextInput,
    AiDropdown
  ]
})
export class ComponentLibraryModule { }
