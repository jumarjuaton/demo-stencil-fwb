/* tslint:disable */
/* auto-generated angular directive proxies */
import { ChangeDetectionStrategy, ChangeDetectorRef, Component, ElementRef, EventEmitter, NgZone } from '@angular/core';
import { ProxyCmp, proxyOutputs } from './angular-component-lib/utils';

import { Components } from '@component-library/dist/components';




export declare interface AiButton extends Components.AiButton {}

@ProxyCmp({
  defineCustomElementFn: undefined,
  inputs: ['circle', 'color', 'disabled', 'iconLeft', 'iconOnly', 'iconRight', 'size', 'spinner', 'type']
})
@Component({
  selector: 'ai-button',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  inputs: ['circle', 'color', 'disabled', 'iconLeft', 'iconOnly', 'iconRight', 'size', 'spinner', 'type']
})
export class AiButton {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
  }
}


export declare interface AiDropdown extends Components.AiDropdown {
  /**
   *  
   */
  changeInput: EventEmitter<CustomEvent<any>>;

}

@ProxyCmp({
  defineCustomElementFn: undefined,
  inputs: ['disabled', 'icon', 'invalid', 'itemText', 'itemValue', 'label', 'menuMheight', 'messageInvalid', 'placeholder', 'selected', 'size']
})
@Component({
  selector: 'ai-dropdown',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  inputs: ['disabled', 'icon', 'invalid', 'itemText', 'itemValue', 'label', 'menuMheight', 'messageInvalid', 'placeholder', 'selected', 'size']
})
export class AiDropdown {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
    proxyOutputs(this, this.el, ['changeInput']);
  }
}


export declare interface AiIcon extends Components.AiIcon {}

@ProxyCmp({
  defineCustomElementFn: undefined,
  inputs: ['color', 'icon', 'size']
})
@Component({
  selector: 'ai-icon',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  inputs: ['color', 'icon', 'size']
})
export class AiIcon {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
  }
}


export declare interface AiModal extends Components.AiModal {
  /**
   *  
   */
  closeButtonClicked: EventEmitter<CustomEvent<any>>;

}

@ProxyCmp({
  defineCustomElementFn: undefined,
  inputs: ['buttons', 'disabled', 'zIndex']
})
@Component({
  selector: 'ai-modal',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  inputs: ['buttons', 'disabled', 'zIndex']
})
export class AiModal {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
    proxyOutputs(this, this.el, ['closeButtonClicked']);
  }
}


export declare interface AiOverlay extends Components.AiOverlay {}

@ProxyCmp({
  defineCustomElementFn: undefined,
  inputs: ['color', 'isshow', 'opacity', 'position', 'zIndex']
})
@Component({
  selector: 'ai-overlay',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  inputs: ['color', 'isshow', 'opacity', 'position', 'zIndex']
})
export class AiOverlay {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
  }
}


export declare interface AiTextInput extends Components.AiTextInput {}

@ProxyCmp({
  defineCustomElementFn: undefined,
  inputs: ['disabled', 'hauled', 'iconEnd', 'iconStart', 'invalid', 'invalidMessage', 'label', 'placeholder', 'readOnly', 'resizable', 'type', 'value']
})
@Component({
  selector: 'ai-text-input',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  inputs: ['disabled', 'hauled', 'iconEnd', 'iconStart', 'invalid', 'invalidMessage', 'label', 'placeholder', 'readOnly', 'resizable', 'type', 'value']
})
export class AiTextInput {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
  }
}
