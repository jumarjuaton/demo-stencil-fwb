#!/usr/bin/env node

const fs = require('fs-extra');
const glob = require('glob-promise');
const path = require('path');
const svgson = require('svgson');

// This is for generating typescript definition for icon type
const prefix = `// This is automatically generated by build-icons
export type IconType = `;
// first, get a list of all of your icons in the source folder
glob(`${__dirname}/iconset/*.svg`)
    // next, read their files, using svgson to parse
    .then(filePaths => Promise.all(filePaths.map((fileName) => {
        return new Promise((resolve) => {
            fs.readFile(fileName, 'utf-8').then((svg) => {
                svgson.parse(svg, {}).then((contents => {
                    resolve({
                        file: fileName,
                        contents
                    });
                }))
            });
        });
    })))
    // write a JSON file inside your component's asset folder for each icon
    .then(files => {
        let arr = [];
        files.forEach((svg) => {
            let file = path.basename(svg.file).slice(0, -4);
            let paths = svg.contents.children
                .filter(child => child.name === 'path')
                .map(child => (child.attributes));
            arr.push(`'${file}'`)
            // Make sure this folder exists!
            let filename = path.resolve(__dirname, `../src/components/ai-icon/assets/${file}.json`);
            fs.writeFileSync(filename, JSON.stringify(paths), 'utf8');
        });
        fs.writeFileSync(path.resolve(__dirname, `../src/components/ai-icon/icon.d.ts`), `${prefix}${arr.join(' | ')};`, 'utf8');
        process.exit(0);
    }).catch(error => {
        console.log(error)
    });