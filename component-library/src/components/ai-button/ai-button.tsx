import {
    Component,
    Host,
    Listen,
    Prop,
    h
} from '@stencil/core';
import { IconType } from "../ai-icon/icon";

@Component({
    tag: 'ai-button',
    styleUrl: 'ai-button.scss',
})

export class AiButton {
    // Color
    @Prop() color? : 'primary' | 'primary-reverse' | 'secondary' | 'secondary-reverse' | 'warning-primary' | 'warning-secondary' | 'tertiary' | 'quarternary' | 'special'  = 'primary';

    // Size
    @Prop() size? : 'xl' | 'lg' | 'md' | 'sm' = 'md';

    // Type
    @Prop() type? : 'button' | 'submit' | 'reset'  = 'button';

    // Disabled
    @Prop({ reflect: true }) disabled ? : boolean = false;

    // IconOnly
    @Prop() iconOnly: IconType = null;

    // IconLeft
    @Prop() iconLeft: IconType = null;

    // IconRight
    @Prop() iconRight: IconType = null;

    // Circle
    @Prop({ reflect: true }) circle? : boolean = false;

    // Spinner
    @Prop({ reflect: true }) spinner? : boolean = false;

    /**
     * Prevent clicks from being emitted from the host
     * when the component is `disabled`.
     */
    @Listen('click', { capture: true })
    handleHostClick(event: Event) {
        if (this.disabled === true) {
            event.stopImmediatePropagation();
        }
    }

    createIcon(className, icon) {
        return <ai-icon class={ className } icon={ icon }></ai-icon>;
    }

    render() {
        let classNames: string = `ai-button ${this.color} ${this.size} ${this.iconOnly? `icon-only` : ``} ${this.circle? `circle` : ``}`;
        let content;
        let spinnerImg = <svg class="spinner" viewBox="0 0 24 25"><circle class="path" cx="12" cy="12" r="8" fill="none" stroke="white" stroke-width="1"></circle></svg>;
  
        /** 
         * Hide button text if icon has been filled/attr is circle
         * else display either right/left icons. Could also be both 
         * or no icon.  
         */
        if ( this.iconOnly != null || this.circle ) {
            content = <div><span id="hidden-content"><slot /></span>{ this.createIcon('', this.iconOnly) }</div>;
        } else {
            if ( this.iconLeft != null && this.iconRight != null ) {
                content = <div>{ this.spinner? spinnerImg : '' }<div>{ this.createIcon('icon-left', this.iconLeft) }<slot />{ this.createIcon('icon-right', this.iconRight) }</div></div>;
            } else if ( this.iconLeft != null ) {
                content = <div>{ this.spinner? spinnerImg : '' }<div>{ this.createIcon('icon-left', this.iconLeft) }<slot /></div></div>;
            } else if ( this.iconRight != null ) {
                content = <div>{ this.spinner? spinnerImg : '' }<div><slot />{ this.createIcon('icon-right', this.iconRight) }</div></div>;
            } else {
                content = <div>{ this.spinner? spinnerImg : '' }<div><slot /></div></div>;
            }
        }

        return ( 
          <Host>
              <button type={ this.type } disabled={ this.disabled } class={ classNames }>{ content }</button> 
          </Host>
        );
    }
}
