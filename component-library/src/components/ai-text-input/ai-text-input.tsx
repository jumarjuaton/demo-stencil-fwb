import { Component, Host, Listen, Prop, h } from '@stencil/core';
import { IconType } from "../ai-icon/icon";

@Component({
	tag: 'ai-text-input',
	styleUrl: 'ai-text-input.scss',
})

export class AiTextInput {
	// Type
	@Prop() type? : string = 'text';

	// Value
	@Prop({ mutable: true }) value? : '';

	// Label
	@Prop() label? : string = 'Label';

	// Placeholder
	@Prop() placeholder? : string = 'Placeholder';

	// Hauled
	@Prop({ reflect: true }) hauled? : boolean = false;

    // Resizable
	@Prop({ reflect: true }) resizable? : boolean = false;

	// IconStart
	@Prop() iconStart: IconType = null;

	// IconEnd
    @Prop() iconEnd: IconType = null;

	// Disabled
	@Prop({ reflect: true }) disabled? : boolean = false;

	// Read Only
	@Prop({ reflect: true }) readOnly? : boolean = false;

	// Invalid
	@Prop({ reflect: true }) invalid? : boolean = false;

	// InvalidMessage
	@Prop() invalidMessage? : string = '';

	/**
	 * Prevent clicks from being emitted from the host
	 * when the component is `disabled`.
	 */
	@Listen('click', { capture: true })
	handleHostClick(event: Event) {
		if (this.disabled === true) {
			event.stopImmediatePropagation();
		}
	}

	createIcon(className, icon) {
        return <ai-icon class={ className } icon={ icon }></ai-icon>;
    }

	render() {
		let isIconStart = (this.iconStart != null && !this.iconEnd);
		let isIconEnd = (!this.iconStart && this.iconEnd != null);
		let classNames: string = `ai-text-input ${this.hauled? `hauled` : ``} ${isIconStart? `icon-start` : ``} ${isIconEnd? `icon-end` : ``} ${this.resizable? `resizable` : ``} ${this.disabled? `disabled` : ``} ${this.invalid? `invalid` : ``}`;

		return ( 
			<Host>
		  		<div class={ classNames }>
		  			<span id="hidden-content"><slot /></span>
				  	<label htmlFor={ this.label }>{ this.label }</label>
				  	<div class="input-group">
				      	<span class="input-group-start">{ isIconStart && this.createIcon('icon-start', this.iconStart) }</span>
				      	<span class="input" role="textbox" contenteditable></span> 
						<input type={ !!this.type ? this.type : 'text' }
							class="input-control"
							name={ this.label.replace(/\s+/g, '-').toLowerCase() } 
							placeholder={ this.placeholder } 
							disabled={ this.disabled }
							readonly={ this.readOnly }
							value={ !!this.value ? this.value : '' }
						/>	
						<span class="input-group-end">{ isIconEnd && this.createIcon('icon-end', this.iconEnd) }</span>	
				  	</div>
				  	<p class="invalid-message">{ !!this.invalidMessage ? this.invalidMessage : 'Please provide a valid ' + this.label.toLowerCase() + '.' }</p> 
				</div>
			</Host>
		);
	}
}
