import { Component, Host, Element, Event, EventEmitter, Prop, Listen, Watch, State, h } from '@stencil/core';
const ResizeObserverPolyfill = require('resize-observer-polyfill');

@Component({
  	tag: "ai-modal",
  	styleUrl: "./ai-modal.scss"
})
export class AiModal {
    // Disabled
    @Prop({ reflect: true }) disabled ? : boolean = false;

  	// Buttons
  	@Prop() buttons?: string;

  	// zIndex
	@Prop() zIndex? : number = 9999;

	@Event() closeButtonClicked: EventEmitter;
	closeButtonClickedHandler(event) {
		this.closeButtonClicked.emit(event.target);
	}

  	// Window element height
  	@Element() el: HTMLElement;
  	window!: HTMLElement;
  	footer!: HTMLElement;
  	private ro: ResizeObserver = new ResizeObserverPolyfill();

 	@State() _buttons: Array<any>;

  	@Watch('buttons')
    	objectDataWatcher(newValue) {
      	if (typeof newValue === 'string') {
          	this._buttons = JSON.parse(newValue);
      	} else {
        	this._buttons = newValue;
      	} 
    }

  	componentWillLoad() {
    	this.objectDataWatcher(this.buttons);
  	}

  	componentDidLoad() {
  		let prevHeight = 0;
  		
		this.ro = new ResizeObserver(entries => {
			for (const entry of entries) {
		    	const height = entry.borderBoxSize?.[0].blockSize;
		    	if (typeof height === 'number' && height !== prevHeight) {
			      	prevHeight = height;
			      	this.applyWindowHeightClass(height);
			    }
			}
		});
		const bodyHeight = this.el.shadowRoot.querySelector<HTMLElement>('.body').offsetHeight;
		bodyHeight >= 182 && this.footer.classList.toggle("top-shadow", true);

		this.ro.observe(document.body);

		// Add a class that changes footer buttons layout to column if .col exists
		const footer = this.el.shadowRoot.querySelector('.footer');
		footer.querySelectorAll('.col').length > 0 && footer.classList.add('col-direction');

  	}

  	disconnectedCallback() {
		this.ro.disconnect();
  	}

  	applyWindowHeightClass(height: number) {
  		let xs = false;
		let sm = false;
		if (height <= 420) xs = true;
		else if (height <= 575) sm = true;
		this.window.classList.toggle("screen-h-xs", xs);
		this.window.classList.toggle("screen-h-sm", sm);
  	}

  	checkButtonTextLength(text) {
	  	return text.length > 20;
	}

  	/**
     * Prevent clicks from being emitted from the host
     * when the component is `disabled`.
     */
    @Listen('click', { capture: true })
    handleHostClick(event: Event) {
        if (this.disabled === true) {
            event.stopImmediatePropagation();
        }
    }

  	render() {
		return (
			<Host>
		  		<div class="ai-modal" tabindex="-1" role="dialog" ref={ (el) => this.window = el as HTMLElement }>
          			<div class="dialog" style={{ 'z-index': `${this.zIndex}` }}>
              			<div class="content">
							<div class="header">
								<div>
                    				<slot name="header" />
		                    		<div onClick={ this.closeButtonClickedHandler.bind(this) }>
		                      			<svg class="icon-x" xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 24 24"><path d="M18.7071 5.29289C19.0976 5.68342 19.0976 6.31658 18.7071 6.70711L6.70711 18.7071C6.31658 19.0976 5.68342 19.0976 5.29289 18.7071C4.90237 18.3166 4.90237 17.6834 5.29289 17.2929L17.2929 5.29289C17.6834 4.90237 18.3166 4.90237 18.7071 5.29289Z" clip-rule="evenodd" fill-rule="evenodd" fill="#0F2C4D"></path><path d="M5.29289 5.29289C5.68342 4.90237 6.31658 4.90237 6.70711 5.29289L18.7071 17.2929C19.0976 17.6834 19.0976 18.3166 18.7071 18.7071C18.3166 19.0976 17.6834 19.0976 17.2929 18.7071L5.29289 6.70711C4.90237 6.31658 4.90237 5.68342 5.29289 5.29289Z" clip-rule="evenodd" fill-rule="evenodd" fill="#0F2C4D"></path></svg>
		              				</div>
		                		</div>
                				<hr />
              				</div>
              				<div class="body"><slot name="content" /></div>
		              		<div class="footer" ref={ (el) => this.footer = el as HTMLElement }>
		                		<slot name="footer" />
		          			</div>
						</div>
					</div>
				</div>
	  		</Host>
		);
  	}
}
