# ai-modal



<!-- Auto Generated Below -->


## Properties

| Property   | Attribute  | Description | Type      | Default     |
| ---------- | ---------- | ----------- | --------- | ----------- |
| `buttons`  | `buttons`  |             | `string`  | `undefined` |
| `disabled` | `disabled` |             | `boolean` | `false`     |
| `zIndex`   | `z-index`  |             | `number`  | `9999`      |


## Events

| Event                | Description | Type               |
| -------------------- | ----------- | ------------------ |
| `closeButtonClicked` |             | `CustomEvent<any>` |


----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
