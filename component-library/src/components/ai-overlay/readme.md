# ai-overlay



<!-- Auto Generated Below -->


## Properties

| Property   | Attribute  | Description | Type                    | Default     |
| ---------- | ---------- | ----------- | ----------------------- | ----------- |
| `color`    | `color`    |             | `string`                | `'#0f2c4d'` |
| `isshow`   | `isshow`   |             | `boolean`               | `undefined` |
| `opacity`  | `opacity`  |             | `number`                | `0.4`       |
| `position` | `position` |             | `"absolute" \| "fixed"` | `'fixed'`   |
| `zIndex`   | `z-index`  |             | `number`                | `9998`      |


----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
