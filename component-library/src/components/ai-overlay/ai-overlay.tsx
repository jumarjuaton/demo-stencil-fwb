import { Component, Host, Prop, Listen, h } from '@stencil/core';

@Component({
	tag: "ai-overlay",
	styleUrl: "./ai-overlay.scss"
})
export class AiOverlay {
	// Isshow
	@Prop({ mutable: true, reflect: true }) public isshow: boolean;

	// Position
	@Prop() position? : 'absolute' | 'fixed' = 'fixed';

	// Color
	@Prop() color? : string = '#0f2c4d';

	// Opacity
	@Prop() opacity? : number = 0.4;

	// zIndex
	@Prop() zIndex? : number = 9998;


	@Listen('closeButtonClicked')
		closeButtonClickedHandler(event: CustomEvent) {
		if (event.target as HTMLElement) {
			this.isshow = false;
		}
	}

	convertHexToRgba(c, o) {
			if(/^#([0-9a-fA-F]{3}){1,2}$/.test(c)){
					if(c.length== 4){
							c= '#'+[c[1], c[1], c[2], c[2], c[3], c[3]].join('');
					}
					c= '0x'+c.substring(1);
					return 'rgba('+[(c>>16)&255, (c>>8)&255, c&255].join(',')+', ' + o +')';
			}
			return '';
		}

	render() {
		let classNames: string = `ai-overlay ${this.isshow? `is-show` : ``} ${this.position }`;
		return (
			<Host>
				<div class={ classNames }>
					<div class="overlay" style={{
						'position': `${this.position}`, 
						'background': `${this.convertHexToRgba(this.color, this.opacity)}`,
						'z-index': `${this.zIndex}`, 
					}} />
					<attr-injector attrs={{ 
							isshow: `${this.isshow}`,
							position: `${this.position}`
						}}>
						<slot />
					</attr-injector>
				</div>
			</Host>
		);
	}
}
