import {  Component, h, Prop, State, Watch } from "@stencil/core";
import { IconType } from "./icon";

/* All our icons are in a 24x24 frame */
const ICON_SIZE = 24
@Component({
    tag: "ai-icon",
    styleUrl: "ai-icon.scss",
})
export class AiIcon {
    /**
    * icon
    */
    @Prop() icon: IconType = null;
    /**
    * size
    */
    @Prop() size: number = 16;
    /**
    * color
    */
    @Prop() color: string = null;
    @State() private pathData: Array<any>;
    async componentWillLoad() {
        try {
            const resp = await fetch(`https://s3.ap-southeast-2.amazonaws.com/designicons.archistar.io/${this.icon}.json`);
            this.pathData = await resp.json();
        } catch (error) {
            console.log(error)
        }
    }

    @Watch('icon')
	async validateIcon() {
		try {
            const resp = await fetch(`https://s3.ap-southeast-2.amazonaws.com/designicons.archistar.io/${this.icon}.json`);
            this.pathData = await resp.json();
        } catch (error) {
            console.log(error)
        }
	}
    
    render() {
        return <svg
            xmlns="http://www.w3.org/2000/svg"
            width={this.size}
            height={this.size}
            viewBox={`0 0 ${ICON_SIZE} ${ICON_SIZE}`}
            >
            {this.pathData.map(path => <path d={path.d} clip-rule={path['clip-rule']} fill-rule={path['fill-rule']} fill={this.color || path.fill} />)}
            </svg>
    }
}
  