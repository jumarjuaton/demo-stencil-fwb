import { Component, Listen, Host, Event, EventEmitter, State, Prop, h } from '@stencil/core';
import { IconType } from "../ai-icon/icon";

@Component({
  tag: 'ai-dropdown',
  styleUrl: 'ai-dropdown.scss',
})

export class AiDropDown {
  private dropDown?: HTMLInputElement;
  // Emitted when the input is changed by user interaction. returns selected item
  @Event() changeInput: EventEmitter;

  // Set default selected item
  @Prop({ reflect: true, mutable: true }) selected: any;

  // Disables the input
  @Prop({ reflect: true }) disabled ? : boolean = false;

  // Sets input label
	@Prop() label? : string = '';

  // Set property of items max height
	@Prop() menuMheight: string;

  // Set property of items’s value - must be primitive. Dot notation is supported
  @Prop() itemValue: string;

  // Set property of items’s text value
  @Prop() itemText: string;

  // Sets the input’s height
  @Prop() size: string = 'small';
  	
  // Sets the input’s placeholder text
  @Prop() placeholder: string;

  // Prepends an icon to the component
  @Prop() icon: IconType = null;

  // Puts the input in a manual error state
  @Prop({ reflect: true }) invalid? : boolean = false;

  // Puts the input in an error state and passes through custom error message.
  @Prop() messageInvalid? : string = '';
     
  @State() showMenu: boolean = false;
  
  // The input’s value
  @State() value: any;

  /* Can be an array of objects or array of strings.
  Objects that have a icon or divider property are considered special cases and generate a list icon or divider
  {
    text: string | number | object,
    value: string | number | object,
    icon: string (icon type),
    divider: boolean,
  }
  
  e.g.
  [
    { 'id': 101, 'name': 'Mark', 'icon': 'globe' },
    { 'id': 102, 'name': 'Smith' }
  ]
  or
  [ 'Mark', 'Smith' ]*/
  @State() items: any[] = [
    { 'id': 101, 'name': 'Mark', 'icon': 'globe' },
    { 'id': 102, 'name': 'Smith' },
    { 'divider': true },
    { 'id': 103, 'name': 'Mark', 'icon': 'globe' },
  ];

  @Listen('click', { target: 'window' })
  handleClick(event) {
    if(!this.dropDown.contains(event.target)){
      this.showMenu = false
    }
  }

  handleMenu(item) {
    this.value = item
    this.selected = item
    this.showMenu = false
    this.changeInput.emit(item);
  }

  handleSelect() {
    this.showMenu = !this.showMenu
  }

  createIcon(icon) {
    return <ai-icon icon={ icon }></ai-icon>;
  }

  renderDefaultItem(item) {
    return typeof item === 'object' ? Object.values(item)[0] : item
  }

  renderIcon(item) {
    return typeof item === 'object' ? item.icon ? this.createIcon(this.icon) : '' : ''
  }

  renderItem(item) {
    return <div onClick={() => this.handleMenu(item)}
      class={ `ai-list-item ${ JSON.stringify(this.selected) === JSON.stringify(item) ? 'selected' : '' }` }
      role='option'
      aria-value={ this.itemValue ? item[this.itemValue] : item }
      aria-selected={ JSON.stringify(this.selected) === JSON.stringify(item) }>

      { typeof item === 'object' ? item.icon ? <span>{ this.renderIcon(item) }</span> : '' : '' }
      { this.itemText ? item[this.itemText] : this.renderDefaultItem(item) }
    </div>
  }

  renderValue(value) {
    if (value) {
      return this.itemValue ? value[this.itemValue] : this.renderDefaultItem(value)
    } else {
      return ''
    }
  }

  componentWillLoad() {
    this.selected ? this.value = this.selected : ''
  }

  render() {
    let classNames: string = `ai-dropdown ${this.showMenu ? 'menu-active' : ''} ${this.size}`;

    return (
      <Host>
        <div class={ classNames } ref={el => this.dropDown = el as HTMLInputElement}>
            {/* to do: icon-start */}
            <ai-text-input disabled={ this.disabled } value={ this.renderValue(this.value) } icon-end={`${this.showMenu ? 'chevron-up' : 'chevron-down'}`}
              placeholder={ this.placeholder } invalid={ this.invalid } message-invalid={ this.messageInvalid }
              label={ this.label } read-only={ true } onClick={() => this.handleSelect()}></ai-text-input>
            <div class={ 'ai-list' } style={ this.menuMheight ? { maxHeight: `${this.menuMheight}` } : {} }>
              {
                this.items ? this.items.map(item => (
                  (!item.divider ? 
                    <div onClick={() => this.handleMenu(item)}
                    class={ `ai-list-item ${ JSON.stringify(this.selected) === JSON.stringify(item) ? 'selected' : '' }` }
                    role='option'
                    aria-value={ this.itemValue ? item[this.itemValue] : item }
                    aria-selected={ JSON.stringify(this.selected) === JSON.stringify(item) }>

                    { typeof item === 'object' ? item.icon ? <span>{ this.renderIcon(item) }</span> : '' : '' }
                    { this.itemText ? item[this.itemText] : this.renderDefaultItem(item) }

                  </div> : <div class={'divider'}><hr /></div>)
                )) : <div class={ `ai-list-item` }></div>
              }
            </div>
        </div>
      </Host>
    );
  }
}
